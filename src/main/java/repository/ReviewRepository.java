package repository;

import domain.Book;
import domain.Review;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

public class ReviewRepository {

    public static void create(Review review) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(review);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void create(Integer bookId, Float score, String comment) {
        Transaction transaction = null;
        Review review = new Review();
        review.setScore(score);
        review.setComment(comment);
        review.setBook(BookRepository.findById(bookId));
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(review);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static List<Review> findAll(){
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from Review";
        List<Review> result = session.createQuery(hql).getResultList();
        session.close();
        return result;
    }


}
