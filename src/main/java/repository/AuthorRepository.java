package repository;

import domain.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class AuthorRepository {


/*    public static void updateAuthor(Author author){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(author);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void deleteAuthor(Integer authorId){
        Transaction transaction = null;
        Author author = new Author();
        author.setAuthorId(authorId);
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // delete the person object
            session.delete(author);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }


    public static Author getAuthor(Long authorId){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Author person = session.find(Author.class, authorId);
            return person;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static List<Author> getAuthors() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Author> persons = session.createQuery("from Author", Author.class).list();
            return persons;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }*/


    public static void create(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(author);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void create(String firstName, String lastName) {
        Transaction transaction = null;
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(author);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }


    public static List<Author> findAll(){
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from Author";
        List<Author> result = session.createQuery(hql).getResultList();
        session.close();
        return result;
    }

    public static Author findById(Integer authorId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from Author WHERE authorId = :authorId";
        List<Author> result = session.createQuery(hql).setParameter("authorId", authorId).getResultList();
        Author author = result.get(0);
        session.close();
        return author;
    }

    public static void deleteById(Integer authorId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "DELETE FROM Author WHERE authorId = :authorId";
        Query query = session.createQuery(hql).setParameter("authorId", authorId);
        Transaction transaction = session.beginTransaction();
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

    public static void update(Integer authorId, String firstName, String lastName) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "UPDATE Author SET firstName = :firstName, lastName = :lastName WHERE authorId = :authorId";
        Query query = session.createQuery(hql);
        query.setParameter("authorId", authorId);
        query.setParameter("firstName", firstName);
        query.setParameter("lastName", lastName);
        Transaction transaction = session.beginTransaction();
        query.executeUpdate();
        transaction.commit();
        session.close();
    }



}
