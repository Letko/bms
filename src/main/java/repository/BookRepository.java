package repository;

import domain.Author;
import domain.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class BookRepository {
/*    public static void createBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(book);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void updateBook(Book book){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(book);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void deleteBook(Book book){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(book);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static Book getBook(Long bookId){
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Book book = session.find(Book.class, bookId);
            return book;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static List<Book> getBooks() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Book> persons = session.createQuery("from Book", Book.class).list();
            return persons;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }*/

    public static void create(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public static void create(String title, String description, Integer authorId) {
        Transaction transaction = null;
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(description);
        book.setAuthor(AuthorRepository.findById(authorId));
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }


    public static List<Book> findAll(){
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from Book";
        List<Book> result = session.createQuery(hql).getResultList();
        session.close();
        return result;
    }

    public static Book findById(Integer bookId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "from Book WHERE bookId = :bookId";
        List<Book> result = session.createQuery(hql).setParameter("bookId", bookId).getResultList();
        Book author = result.get(0);
        session.close();
        return author;
    }

    public static void deleteById(Integer bookId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "DELETE FROM Book WHERE bookId = :bookId";
        Query query = session.createQuery(hql).setParameter("bookId", bookId);
        Transaction transaction = session.beginTransaction();
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

    public static void update(Integer bookId, String title, String description, Integer authorId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "UPDATE Book SET title = :title, description = :description, authorId = :authorId WHERE bookId = :bookId";
        Query query = session.createQuery(hql);
        query.setParameter("bookId", bookId);
        query.setParameter("title", title);
        query.setParameter("description", description);
        query.setParameter("authorId", authorId);
        Transaction transaction = session.beginTransaction();
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

}




